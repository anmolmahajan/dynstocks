package com.cuddleinstyle.class4;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class Suggestions extends AppCompatActivity implements SuggestionInterface{
    ListView lv;
    ArrayList<Stock> data;
    CustomAdapter adapter;
    LayoutInflater inflator;
    ProgressDialog pd;
    TextView tv;
    String sendTickers = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suggestions);
        data = new ArrayList<>();
        lv = (ListView) findViewById(R.id.sugg_lv);
        tv = (EditText) findViewById(R.id.sugg_tv);
        adapter = new CustomAdapter(this,data);
        inflator = getLayoutInflater();
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                sendTickers += adapter.getItem(position).handle.toString() + "^";
                Toast.makeText(Suggestions.this,adapter.getItem(position).name,Toast.LENGTH_SHORT).show();

            }
        });
        tv.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                    String urlString = urlgiver(tv);
                    SuggestionAsync task = new SuggestionAsync();
                    task.listener = Suggestions.this;
                    task.execute(urlString);
                    pd = new ProgressDialog(Suggestions.this);
                    pd.setTitle("Fetching");
                    pd.setMessage("Fetching the data. Please wait.");
                    pd.show();


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });



    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent();
        i.putExtra("names",sendTickers);
        setResult(1, i);
        super.onBackPressed();
    }

    public String urlgiver(TextView t){
        String res = "http://d.yimg.com/autoc.finance.yahoo.com/autoc?query=";
        String s = t.getText().toString();
        if(s != null && s.length() != 0){
            res += s;
        }
        res += "&callback=YAHOO.Finance.SymbolSuggest.ssCallback";
        return res;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_suggestions, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void suggestionTaskonComplete(Stock stock[]) {
        if(stock != null){
            data.clear();
            for (Stock s : stock) {
                data.add(s);
            }
        }
        adapter.notifyDataSetChanged();
        pd.dismiss();
    }
}
