package com.cuddleinstyle.class4;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

/**
 * Created by anmol on 13/9/15.
 */
public class SuggestionAsync extends AsyncTask<String,Void,Stock[]>{

    SuggestionInterface listener;

    @Override
    protected void onPostExecute(Stock[] stocks) {
        if(listener != null){
            listener.suggestionTaskonComplete(stocks);
        }
    }

    @Override
    protected Stock[] doInBackground(String... params) {
        String urlString = params[0];
        try{
            URL url = new URL(urlString);
            HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            InputStream data = urlConnection.getInputStream();
            Scanner s = new Scanner(data);
            StringBuffer output = new StringBuffer();
            while (s.hasNext()) {
                output.append(s.nextLine());
            }
            s.close();
            urlConnection.disconnect();
            return parseJson(output.toString());
        }catch(MalformedURLException e){
            return null;
        }catch(IOException e){
            return null;
        }
    }

    public Stock[] parseJson(String jsonString){
        String s = jsonString.substring(jsonString.indexOf('(') + 1,jsonString.lastIndexOf(')'));
        Log.i("jsonptojson",s);
        try{
            JSONObject object = new JSONObject(s);
            JSONObject resultSet = object.getJSONObject("ResultSet");
            JSONArray resultarr = resultSet.getJSONArray("Result");
            Stock[] output = new Stock[resultarr.length()];
            for (int i = 0; i < resultarr.length(); i++) {
                JSONObject company = resultarr.getJSONObject(i);
                Stock st = new Stock();
                st.name = company.getString("name");
                st.handle = company.getString("symbol");
                st.value = "";
                output[i] = st;
            }
            return output;
        }catch(JSONException e){
            return null;
        }
    }
}
